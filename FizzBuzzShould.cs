using NUnit.Framework;

namespace FizzBuzzKata
{
    public class FizzBuzzShould
    {
        private FizzBuzz _fizzBuzz;
        
        [SetUp]
        public void SetUp()
        { 
            _fizzBuzz = new FizzBuzz();     
        }
        
        [TestCase(1, "1")]
        [TestCase(2, "2")]
        [TestCase(4, "4")]
        [TestCase(73, "73")]
        [TestCase(101, "101")]
        public void return_string_when_input_is_not_multiple_of_3_or_5(int input, string output)
        {
            string result = _fizzBuzz.Get(input);
            
            Assert.AreEqual(output, result);
        }
        
        [TestCase(3)]
        [TestCase(6)]
        [TestCase(9)]
        [TestCase(9)]
        [TestCase(18)]
        [TestCase(39)]
        public void return_Fizz_string_when_input_is_multiple_of_3(int input)
        {
            string result = _fizzBuzz.Get(input);
            
            Assert.AreEqual("Fizz", result);
        }

        [TestCase(5)]
        [TestCase(10)]
        [TestCase(20)]
        [TestCase(860)]
        [TestCase(777440)]
        public void return_Buzz_when_input_is_multiple_of_5(int input)
        {
            string result = _fizzBuzz.Get(input);
            
            Assert.AreEqual("Buzz", result);
        }

        [TestCase(15)]
        [TestCase(30)]
        [TestCase(45)]
        [TestCase(180)]
        [TestCase(360)]
        public void return_FizzBuzz_when_input_is_multiple_of_3_and_5(int input)
        {
            string result = _fizzBuzz.Get(input);
            
            Assert.AreEqual("FizzBuzz", result);
        }
    }
}